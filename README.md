# classification-profiles

Repository to store classification profiles for the
[classification-toolkit](https://gitlab.com/flywheel-io/public/classification-toolkit).

## Contributing

Profiles are stored under the [profiles](./profiles) directory.

The default profile is [`main.yaml`](./profiles/main.yaml), which
includes the other profiles defined in the same folder.

### Pre-requisites

This project uses `poetry` to manage virtual environments, and `pre-commit`
to perform linting and validation on scripts and profiles.

* Install [poetry](https://python-poetry.org/docs/#installation)
* Install [pre-commit](https://pre-commit.com/)

### Making a change

1. Initialize virtual environment with `poetry install`
2. Install pre-commit hooks with `pre-commit install`
3. Change to a new branch with a helpful name, e.g.
`git checkout -b <helpful_name>`
4. Make your relevant changes
5. Create a merge request.

#### Releasing a new version

CI will automatically create a release Merge Request if you run a pipeline on `main`
with the variable `RELEASE` set to the desired version.

As long as that MR looks okay, go ahead and merge it, that will do the following:

1. Tag the repo with the desired release tag
2. Create a Release with a changelog from the merged MR titles
3. Create a downstream job to update the classification-profiles submodule on
   [file-classifier](https://gitlab.com/flywheel-io/flywheel-apps/file-classifier)

The downstream job in `3` will create an MR on file-classifier, which you can then merge
and release in the same way (via a `RELEASE` pipeline)
