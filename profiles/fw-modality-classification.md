# Flywheel classification definition

## MR

```json
    {
        "Intent":
        [
            "Localizer",
            "Shim",
            "Calibration",
            "Fieldmap",
            "Structural",
            "Functional",
            "Screenshot",
            "Non-Image",
            "Spectroscopy"
        ],
        "Features":
        [
            "Quantitative",
            "Multi-Shell",
            "Multi-Echo",
            "Multi-Flip",
            "Multi-Band",
            "Steady-State",
            "3D",
            "Compressed-Sensing",
            "Eddy-Current-Corrected",
            "Fieldmap-Corrected",
            "Gradient-Unwarped",
            "Motion-Corrected",
            "Physio-Corrected",
            "Derived",
            "In-Plane",
            "Phase",
            "Magnitude",
            "2D",
            "AAscout",
            "Spin-Echo",
            "Gradient-Echo",
            "EPI",
            "WASSR",
            "FAIR",
            "FAIREST",
            "PASL",
            "EPISTAR",
            "PICORE",
            "pCASL",
            "MPRAGE",
            "MP2RAGE",
            "FLAIR",
            "SWI",
            "QSM",
            "RMS",
            "DTI",
            "DSI",
            "DKI",
            "HARDI",
            "NODDI",
            "Water-Reference",
            "Transmit-Reference",
            "SBRef",
            "Uniform",
            "Singlerep",
            "QC",
            "TRACE",
            "FA",
            "MIP",
            "Navigator",
            "Contrast-Agent",
            "Phase-Contrast",
            "TOF",
            "VASO",
            "iVASO",
            "DSC",
            "DCE",
            "Task",
            "Resting-State",
            "PRESS",
            "STEAM",
            "M0",
            "Phase-Reversed",
            "Spiral",
            "SPGR",
            "Control",
            "Label"
        ],
        "Measurement":
        [
            "B0",
            "B1",
            "T1",
            "T2",
            "T2*",
            "PD",
            "MT",
            "Perfusion",
            "Diffusion",
            "Susceptibility",
            "Fingerprinting",
            "MRA",
            "CEST",
            "T1rho",
            "SVS",
            "CSI",
            "EPSI",
            "BOLD",
            "Phoenix"
        ]
    }
```

## CT

```json
    {
        "Anatomy":
        [
            "Head",
            "Neck",
            "Chest",
            "Abdomen",
            "Pelvis",
            "Upper Extremities",
            "Lower Extremities",
            "Whole Body"
        ],
        "Scan Type":
        [
            "Localizer",
            "AC",
            "Standard",
            "Original",
            "Derived",
            "Synthetic"
        ],
        "Contrast":
        [
            "No Contrast",
            "Contrast",
            "Arterial Phase",
            "Portal Venous Phase",
            "Delayed/Equilibrium Phase"
        ],
        "Scan Orientation":
        [
            "Axial",
            "Sagittal",
            "Coronal"
        ]
    }
```

## PET

```json
    {
        "Anatomy":
        [
            "Head",
            "Neck",
            "Chest",
            "Abdomen",
            "Pelvis",
            "Upper Extremities",
            "Lower Extremities",
            "Whole Body"
        ],
        "Processing":
        [
            "Motion Corrected",
            "Averaged",
            "Filtered",
            "Attenuation Corrected"
        ],
        "Type":
        [
            "Dynamic",
            "Static",
            "Gated"
        ],
        "Tracer":
        [
            "FDG",
            "FES",
            "Zr-CD8",
            "Zr-HER",
            "GTP1",
            "AV45",
            "PIPB",
            "mAb"
        ],
        "Isotope":
        [
            "Ga68",
            "F18",
            "Cu64",
            "N13",
            "O15",
            "C11",
            "I124",
            "Zr89"
        ]
    }

```
