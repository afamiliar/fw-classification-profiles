from pathlib import Path

import pytest
from fw_classification.classify import Profile

PROFILES_ROOT = Path(__file__).parents[1] / "profiles"


@pytest.mark.parametrize(
    "profile_file",
    [
        "MR.yaml",
    ],
)
def test_validate_profiles(profile_file):
    profile_path = PROFILES_ROOT / profile_file
    profile = Profile(profile_path)
    assert profile.errors == []
